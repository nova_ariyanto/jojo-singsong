package com.siwa.jojo

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.siwa.jojo.Adapter.MusicAdapter
import com.siwa.jojo.Adapter.MusicListener
import android.media.MediaPlayer
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.android.gms.ads.*

import com.siwa.jojo.domain.responseMusic
import com.siwa.jojo.rest.ApiClient
import com.siwa.jojo.rest.ApiInterface
import kotlinx.android.synthetic.main.activity_music.*
import org.jetbrains.anko.support.v4.onRefresh
import java.io.IOException

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListMusic : AppCompatActivity(),MusicListener {

    var mPlayer = MediaPlayer()
    private var interstitialAd: InterstitialAd? = null
    var playing = false
    lateinit var mAdView : AdView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)

        createInterstitial()
        refresh()
        swiperefresh.onRefresh{
            refresh()
            swiperefresh.isRefreshing = false
        }
        var id_ads = getString(R.string.banner_ad_unit_id);
        MobileAds.initialize(this,id_ads)

        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)
        mAdView.adListener = object: AdListener() {
            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Log.d("ads","onload")
            }

            override fun onAdFailedToLoad(errorCode : Int) {
                // Code to be executed when an ad request fails.
                Log.d("ads","onfailed to load")
            }

            override fun onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Log.d("ads","on ad opened")
            }

            override fun onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Log.d("ads","onadleft application")
            }

            override fun onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
                Log.d("ads","onadclosed")
            }
        }
    }

    fun refresh(){
        var getResponse: ApiInterface
        getResponse = ApiClient.getClient().create(ApiInterface::class.java)
        val call: Call<responseMusic>
        call = getResponse.music
        call.enqueue(object : Callback<responseMusic> {
            override fun onFailure(call: Call<responseMusic>, t: Throwable) {
                Log.d("Failure", t.message.toString() + " --")
            }
            override fun onResponse(call: Call<responseMusic>, response: Response<responseMusic>) {
                Log.d("response code",response.code().toString()+" -- ")
                if (response.isSuccessful) {
                    val notif = response.body()?.data
                    if (notif != null) {
                        Log.d("tag",notif.toString())
                        val heroesAdapter = MusicAdapter(notif, this@ListMusic)

                        rvMain.apply {
                            //            layoutManager = GridLayoutManager(this@MainActivity, 3)
                            layoutManager = LinearLayoutManager(this@ListMusic)
                            adapter = heroesAdapter
                        }
                        txt_notfound.visibility = View.INVISIBLE
                        rvMain.visibility = View.VISIBLE
//                        mView.showInformasi(notif)
//                        mView.hideloading()
                    }else{
//                        mView.placeholder()
                        txt_notfound.visibility = View.VISIBLE
                        rvMain.visibility = View.INVISIBLE
                    }
                }else{
//                    mView.placeholder()
                    txt_notfound.visibility = View.VISIBLE
                    rvMain.visibility = View.INVISIBLE
                }
            }
        });
    }

    override fun onBackPressed() {
        super.onBackPressed()
        showInterstitial()
        mPlayer.stop()
        mPlayer.reset()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main,menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.getItemId()

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            var intent =  Intent(this@ListMusic,AboutActivity::class.java);
            startActivity(intent)
//            Toast.makeText(this@MainActivity, "Action clicked", Toast.LENGTH_LONG).show()
        }else if(id == R.id.action_share){
            val shareBody = "Here is the share content body"
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
            startActivity(Intent.createChooser(sharingIntent, "Share"))
        }
        return true
    }
    override fun onMusicClick(music: responseMusic.Music) {

            Player(music.url.toString())
//            Toast.makeText(this, "hero clicked ${music.deskripsti}", Toast.LENGTH_SHORT).show()
        }

    fun Player(url: String) {
        val uri = url
        if (playing == true){
            mPlayer.stop()
            mPlayer.reset()
            playing = false
            Log.d("stop play","yes")
        }
        try {
            mPlayer.setDataSource(uri)

            mPlayer.prepare()

            // Start playing audio from http url
            mPlayer.start()
            playing = true
            // Inform user for audio streaming
//            Toast.makeText(applicationContext, "Playing", Toast.LENGTH_SHORT).show()
        } catch (e: IOException) {
            // Catch the exception
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: SecurityException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }

        mPlayer.setOnCompletionListener(MediaPlayer.OnCompletionListener {
//            Toast.makeText(applicationContext, "End", Toast.LENGTH_SHORT).show()
        })
    }
    fun createInterstitial() {
        Log.d("ads","Create interstitial")
        interstitialAd = InterstitialAd(this)
        var id_ads_interstitial : String
        id_ads_interstitial = getString(R.string.intertitial_ad_unit_id);

        interstitialAd?.setAdUnitId(id_ads_interstitial) // Ganti sesuai dengan kode interstitial ads anda
        loadInterstitial()
    }

    fun loadInterstitial() {
        Log.d("ads","load interstitial")
        val interstitialRequest = AdRequest.Builder().build()
        interstitialAd?.loadAd(interstitialRequest)
    }

    fun showInterstitial() {
        Log.d("ads","Show interstitial")
        if (interstitialAd?.isLoaded()!!) {
            interstitialAd?.show()
            interstitialAd?.setAdListener(object : AdListener() {
                override fun onAdLoaded() {
                    // not call show interstitial ad from here
                }

                override fun onAdClosed() {
                    loadInterstitial()

                    ////////////////////////////////
                    val intent = Intent(this@ListMusic, MainActivity::class.java)
                    startActivity(intent)
                    ////////////////////////////////
                }
            })
        } else {
            loadInterstitial()
            Log.d("ads","hide interstitial")
            ////////////////////////////////
            val inte = Intent(this@ListMusic, MainActivity::class.java)
            startActivity(inte)
            ////////////////////////////////
        }
    }

}