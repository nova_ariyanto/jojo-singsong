package com.siwa.jojo.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siwa.jojo.R
import com.siwa.jojo.domain.responseCard
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_card.view.*


class HeroAdapter(private val heroes: List<responseCard.Card>,
                  private val mOnclick:(card:responseCard.Card)->Unit) : RecyclerView.Adapter<HeroHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): HeroHolder {
        return HeroHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_card, viewGroup, false))
    }

    override fun getItemCount(): Int = heroes.size

    override fun onBindViewHolder(holder: HeroHolder, position: Int) {
        holder.bindHero(heroes[position], mOnclick)
    }
}

class HeroHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun bindHero(card: responseCard.Card, mOnclick: (card: responseCard.Card) -> Unit ) {
        with(itemView){
                        txtTitle.text = card.name
                        Picasso.get().load(card.image).into(imgHeroes)
                        setOnClickListener {
                            mOnclick(card)
                        }

        }

    }
}