package com.siwa.jojo.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.siwa.jojo.R
import com.siwa.jojo.domain.responseMusic
import kotlinx.android.synthetic.main.item_music.view.*

class MusicAdapter(private val heroes: List<responseMusic.Music>,
                  private val listenere: MusicListener) : RecyclerView.Adapter<MusicHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MusicHolder {
        return MusicHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_music, viewGroup, false))
    }

    override fun getItemCount(): Int = heroes.size

    override fun onBindViewHolder(holder: MusicHolder, position: Int) {
        holder.bindMusic(heroes[position], listener = listenere)
    }
}

interface MusicListener {
    fun onMusicClick(music: responseMusic.Music)
}

class MusicHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val title = view.title

    fun bindMusic(music: responseMusic.Music, listener: MusicListener) {
        title.text = music.judul

        itemView.setOnClickListener { listener.onMusicClick(music) }
    }
}