package com.siwa.jojo;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.android.gms.ads.*;

public class PlayVideoActivity extends AppCompatActivity {
    String url;
    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);

        String id_ads = getString(R.string.banner_ad_unit_id);
        MobileAds.initialize(this,id_ads);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Log.d("ads","onadloaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.d("ads","onadfailedtoload");
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Log.d("ads","onadopened");
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Log.d("ads","onadleftapp");
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
                Log.d("ads","onadclose");
            }
        });


        SharedPreferences mSettings = getSharedPreferences("sharep", Context.MODE_PRIVATE);
        url = mSettings.getString("url", "");
//        Toast.makeText(this, url+" : url", Toast.LENGTH_SHORT).show();
        VideoView videoView = (VideoView)findViewById(R.id.myVideo);
         Uri vidUri = Uri.parse(url);
        videoView.setVideoURI(vidUri);
        videoView.start();

        MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(videoView);

        videoView.setMediaController(vidControl);
    }
}
