package com.siwa.jojo.domain

import com.google.gson.annotations.SerializedName

data class responseCard(@SerializedName("response")val response:String?,
                        @SerializedName("status")val status:String?,
                        @SerializedName("title")val title:String?,
                        @SerializedName("data")val data:List<Card>) {

    data class Card(
            @SerializedName("id") val id: String?,
            @SerializedName("name") val name: String?,
            @SerializedName("image") val image: String?,
            @SerializedName("type") val type: String?,
            @SerializedName("url") val url: String?
    )
}