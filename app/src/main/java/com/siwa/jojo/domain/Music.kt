package com.siwa.jojo.domain

import com.google.gson.annotations.SerializedName

data class responseMusic(@SerializedName("response")val response:String?,
                        @SerializedName("status")val status:String?,
                        @SerializedName("title")val title:String?,
                        @SerializedName("data")val data:List<Music>) {
    data class Music(
            @SerializedName("id") val id: String?,
            @SerializedName("judul") val judul: String?,
            @SerializedName("deskripsi") val deskripsti: String?,
            @SerializedName("url") val url: String?
    )
}