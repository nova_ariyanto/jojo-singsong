package com.siwa.jojo.rest;



import com.siwa.jojo.domain.responseCard;
import com.siwa.jojo.domain.responseMusic;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("list_card.json")
    Call<responseCard> getCard();
    @GET("list_music.json")
    Call<responseMusic> getMusic();
}
