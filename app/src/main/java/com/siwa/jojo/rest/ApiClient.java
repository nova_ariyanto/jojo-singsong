package com.siwa.jojo.rest;
import android.util.Log;

import com.siwa.jojo.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class ApiClient {
    private static Retrofit retrofit = null;
    public static final String base_Url = BuildConfig.SERVER_URL;
    public static Retrofit getClient(){
//        Log.d("url test",base_Url+"");
        if(retrofit==null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_Url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}