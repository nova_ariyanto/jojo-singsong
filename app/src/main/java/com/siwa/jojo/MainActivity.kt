package com.siwa.jojo

import android.content.Context
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.android.gms.ads.*
import com.siwa.jojo.Adapter.HeroAdapter

import com.siwa.jojo.domain.responseCard
import com.siwa.jojo.domain.responseMusic
import com.siwa.jojo.rest.ApiClient
import com.siwa.jojo.rest.ApiInterface
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import org.jetbrains.anko.support.v4.onRefresh

class MainActivity : AppCompatActivity() {
    private var interstitialAd: InterstitialAd? = null
    lateinit var mAdView : AdView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createInterstitial();
        refresh()

        swiperefresh.onRefresh{
            refresh()
            swiperefresh.isRefreshing = false
        }
            var id_ads = getString(R.string.banner_ad_unit_id);
            MobileAds.initialize(this,id_ads)

        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        mAdView.adListener = object: AdListener() {
            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Log.d("ads","onload")
            }

            override fun onAdFailedToLoad(errorCode : Int) {
                // Code to be executed when an ad request fails.
                Log.d("ads","onfailed to load")
            }

            override fun onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Log.d("ads","on ad opened")
            }

            override fun onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Log.d("ads","onadleft application")
            }

            override fun onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
                Log.d("ads","onadclosed")
            }
        }

    }


    fun refresh(){
        var getResponse: ApiInterface
        getResponse = ApiClient.getClient().create(ApiInterface::class.java)
        val call: Call<responseCard>
        call = getResponse.card
        call.enqueue(object : Callback<responseCard> {
            override fun onFailure(call: Call<responseCard>, t: Throwable) {
                Log.d("Failure", t.message.toString() + " --")
            }

            override fun onResponse(call: Call<responseCard>, response: Response<responseCard>) {
                Log.d("response code",response.code().toString()+" -- ")
                if (response.isSuccessful) {
                    val notif = response.body()?.data
                    if (notif != null) {
                        val heroesAdapter = HeroAdapter(notif){    card -> onklik(card) }

                        rvMain.apply {
                            //            layoutManager = GridLayoutManager(this@MainActivity, 3)
                            layoutManager = LinearLayoutManager(this@MainActivity)
                            adapter = heroesAdapter
                        }
                        txt_notfound.visibility = View.INVISIBLE
                        rvMain.visibility = View.VISIBLE
                        Log.d("list card",notif.toString())
//                        mView.showInformasi(notif)
//                        mView.hideloading()
                    }else{
//                        mView.placeholder()
                        txt_notfound.visibility = View.VISIBLE
                        rvMain.visibility = View.INVISIBLE
                    }
                }else{
//                    mView.placeholder()
                    txt_notfound.visibility = View.VISIBLE
                    rvMain.visibility = View.INVISIBLE
                }
            }
        });
    }

     fun onklik(card: responseCard.Card) {
        var nama :String;
        nama = card.type.toString();
        if(nama == "list"){
            showInterstitial1()
        }else{
            val mSettings = getSharedPreferences("sharep", Context.MODE_PRIVATE)
            val editor = mSettings.edit()
            editor.putString("url", card.url.toString())
            editor.apply()
            showInterstitial()
//            Toast.makeText(this, "hero clicked ${card.name}", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main,menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.getItemId()
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            var intent =  Intent(this@MainActivity,AboutActivity::class.java);
            startActivity(intent)
//            Toast.makeText(this@MainActivity, "Action clicked", Toast.LENGTH_LONG).show()
        }else if(id == R.id.action_share){
            val shareBody = getString(R.string.share_body_text)
            val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
            startActivity(Intent.createChooser(sharingIntent, "Share"))
        }

        return true
    }

    fun createInterstitial() {
        Log.d("ads","Create interstitial")
        interstitialAd = InterstitialAd(this)
        var id_ads_interstitial : String
        id_ads_interstitial = getString(R.string.intertitial_ad_unit_id);
        interstitialAd?.setAdUnitId(id_ads_interstitial) // Ganti sesuai dengan kode interstitial ads anda
        loadInterstitial()
    }

    fun loadInterstitial() {
        Log.d("ads","load interstitial")
        val interstitialRequest = AdRequest.Builder().build()
        interstitialAd?.loadAd(interstitialRequest)
    }

    fun showInterstitial() {
        Log.d("ads","Show interstitial")
        if (interstitialAd?.isLoaded()!!) {
            interstitialAd?.show()
            interstitialAd?.setAdListener(object : AdListener() {
                override fun onAdLoaded() {
                    // not call show interstitial ad from here
                }

                override fun onAdClosed() {
                    loadInterstitial()

                    ////////////////////////////////
                    val intent = Intent(this@MainActivity, PlayVideoActivity::class.java)
                    startActivity(intent)
                    ////////////////////////////////
                }
            })
        } else {
            loadInterstitial()
            Log.d("ads","hide interstitial")
            ////////////////////////////////
            val inte = Intent(this@MainActivity, PlayVideoActivity::class.java)
            startActivity(inte)
            ////////////////////////////////
        }
    }
    fun showInterstitial1() {
        Log.d("ads","Show interstitial")
        if (interstitialAd?.isLoaded()!!) {
            interstitialAd?.show()
            interstitialAd?.setAdListener(object : AdListener() {
                override fun onAdLoaded() {
                    // not call show interstitial ad from here
                }

                override fun onAdClosed() {
                    loadInterstitial()

                    ////////////////////////////////
                    val intent = Intent(this@MainActivity, ListMusic::class.java)
                    startActivity(intent)
                    ////////////////////////////////
                }
            })
        } else {
            loadInterstitial()
            Log.d("ads","hide interstitial")
            ////////////////////////////////
            val inte = Intent(this@MainActivity, ListMusic::class.java)
            startActivity(inte)
            ////////////////////////////////
        }
    }

}
